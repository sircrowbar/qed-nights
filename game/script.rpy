﻿# You can place the script of your game in this file.

# Declare images below this line, using the image statement.
# eg. image eileen happy = "eileen_happy.png"

image black = "#000000"
image white = "#FFFFFF"
image red = "#FF0000"
image bg oc1 = "images/bg/oc1.jpg"
image bg oc2 = "images/bg/oc2.jpg"
image bg oc3 = "images/bg/oc3.jpg"
image bg pldoor = "images/bg/ploutside.jpg"
image bg poster = "images/bg/poster.jpg"
image bg plinside = "images/bg/pilotlight1.jpg"
image bg plstage = "images/bg/pilotlight2.jpg"
image bg plshow = "images/bg/pilotlightshow.jpg"
image bg battle = "images/bg/doom.gif"

image item signup = "images/bg/list.jpg"
image item banana = "images/bg/banana.jpg"

image cena stand = "images/cenasmall.png"

# Declare characters used by this game.
define n = Character('Newbie', color="#c8ffc8")


# The game starts here.
label start:


    $ artPoints = 0
    $ drunkPoints = 0
    $ cenaHP = 4
    $ beer = "beer"
    
    scene bg oc2
    with fade

    play music "music/happy.mp3"

    n "Tonight's the night!"

    n "Monday night! Comedy Night!"
    n "No, even better: it's QED Night!"
    n "I've been waiting all week for this!"
    n "I've been to the show a few times in the past, but this is going to be my first time participating as an actual performer. On stage and everything!"


    scene bg oc3
    with dissolve

    n "Of course I'm excited about it. This is my first step into the wide world of Knoxville Comedy, an amazing and constantly growing scene."

    n "And I love the idea too, since there's a new topic to make the funny with every week."

    n "But I'm a little nervous too. What if I bomb?"

    n "No, I can't think that now. Be positive! It'll be fine! It's going to be great!"

    n "Everything will be great!"

    n "I keep telling myself these things over and over again as I walk through the Old City."

    scene bg pldoor
    with dissolve

    n "Well, here we are: The Pilot Light! Knoxville's Pearl!"

    n "(Actually, the Knoxville Pearl is a few doors down, but don't tell them that.)"

    n "Let's just make sure we are in the right place."

    scene bg poster
    with dissolve

    n "Aha! A poster! With comedy on it! Perfect!"

    scene bg pldoor
    with dissolve

    n "Behind these wooden doors lie my new, comedic destiny!"

    n "..."

    n "I'm not sure I want to know my comedic destiny."

    n "Maybe my comedic destiny is, like, REALLY BAD."

    n "No, surely not. But... Maybe?!?"

    n "My self-esteem is in serious fluctuation here!"

    n "... Let me rethink this."

label pilotlightoutside:
    menu:
        "What, are you kidding? JUST GO IN!":
            n "Screw it! You only live an inderterminable amount of time depending on your biologic makeup!"

        "On second thought....":
            n "But thou must! No second thoughts! I'm overriding these emotions and dragging them with me on this cosmic railroad!"

            n "... I'm pretty sure that was a Grateful Dead song, but I'm going to do it anyway! I'm going in!"

    n "Let's do this!"

label pilotlight:

    scene bg plinside
    with dissolve

    play music "music/whic.mp3"

    n "It looks like everyone is about to get started today."

    n "I see the host for this week's show and ask if there are any available spots."

    show item signup

    n "Wow, that's a lot of names! But there's one at the top just waiting for!"

    n "It's number two, which makes me even more nervous, if that is possible."

    n "I take a deep breath and sign my name in cold, black ink."

    hide item

    n "The host takes one look at it and gives me the thumbs up sign. Success! Now it's time for the waiting game!"

    n "..."

    n "Waiting kind of sucks. I should get a beer in the meantime."

    menu:
        "Parched Blues Ribboned":
            $ drunkPoints += 1
            $ beer = "PBR"
            n "PBR! I feel my hipster beard growing with every sip."

            n "Wait, maybe it is growing? ... I should double check."

        "Mallar Highlights":
            $ drunkPoints += 1
            $ beer = "Highlights"
            n "Ah, yes, Mallar Highlights! The Welch's Sparkling Grape of Beers!"

        "Rhyner Bock":
            $ drunkPoints += 1
            $ beer = "Rhyner Bock" 
            n "A Rhyner Bock! It tastes exactly like Comedy! Nice!"

        "Whatever the audience is screaming at the host":
            $ drunkPoints += 1
            $ beer = "Beer Flavored Beer"
            n "Oh a nice (NOUN) Beer, with a smooth (ADJECTIVE) taste! I am (SOME SORT OF SEMI-EUPHORIC FEELING) it!"

        "Never Mind":
            n "On Second thought, nah. I'm good. Maybe later!"


    n "As I leave the area, the bartender slides me a blank piece of paper and a pen."

    "Bartender" "You should draw a banana here."

    n "Um, okay?"

    "Bartender" "You know you want to."

    n "Oh. Um, well, should I?"

    menu:
        "Sure, why not? It's for a good, banana-related, cause I'm sure.":
            $ artPoints += 1
            show item banana at truecenter

            n "I think I did a pretty good job on this banana!"

            n "It really accentuates the plight of the fruit. Y'know... Rowing? Up stream? For the winter? Maybe?"

            n "Anyway, A+! Would banana again!"

            hide item

        "I hate potassium-based drawings":
            n "Nah. I have no desire to recreate Velvet Underground covers."

            n "I respectfully decline the banana drawing proposal"

    n "I take another look around."


label stage:

    scene bg plstage
    with dissolve

    n "There's a few other people milling about the place, talking in groups over various things."

    n "A couple of people are hurrily scrawling something in a notebook, panicked over getting something done for the current topic."

    n "(I'm glad I prepared in advance now! I wouldn't want to think how hard it would be to do that! Or something evem more complicated, like a game!)"

    n "They don't have much time though. It's not long after that the host climbs onto stage and the lights go down."

    scene bg plshow
    with dissolve

    "Host" "Hello everyone and welcome to QED!"

    n "Polite clapping eminates throughout the Pilot Light. It has begun!"

    n "The first comedian gets on stage and is just killer from beginning to end! I was starting to get conscious of how hard I was laughing at it."

    n "There was this one bit about a certain monkey wizard where he... Well, you'd have to be there for it."

    n "It's hysterical. Take my word for it."

    n "But before I know it, I hear the Host call out my name."

    n "Oh snap! It's my turn! I muster up my courage and make my way onto the stage!"

    n "I get up on the stage and take a look towards the crowd. Where there that many people when I came in? There's so many!"

    n "Alright, calm down, me. You can do this! Just do what you were going to do and..."

    n "... Wait, what was it I was going to do?"

    menu:
        "Perform a blistering kazoo solo":
            $ artPoints += 1
            stop music
            play music "music/kazoo1.mp3"
            n "Please look to your host to experience an incredibly kazoo solo."

        "Perform a EXTREMELY blistering kazoo solo":
            $ artPoints += 2
            stop music
            play music "music/kazoo2.mp3"
            n "Please look to your host to experience a SUPER BLISTERING KAZOO SOLO."

    n "You may now resume looking at the game screen."

    play music "music/whic.mp3"

    n "As the sweat pours off my body and the kazoo at the end of the performance, I look out towards the crowd..."

    n "Clapping! Yes! They liked it! I think they liked it. I can't really tell over the buzzing in my ear from the kazoo."

    n "But hey, I'll count it!" 

    n "I return to my seat with a smile, both relieved and happy to finally be able to perform at QED."

    n "There's still quite a few more comedians to go. I was pretty early in the set!"

    if drunkPoints == 0:
        n "Maybe I should get a drink now?"
    else:
       n "Should I grab another drink?"

    menu:
        "Duhhh, yes?!":
            $ drunkPoints += 1
            n "I order another [beer], "
        "There's a lot of comedians after me. Maybe I should have... SEVERAL!":
            $ drunkPoints += 2
            n "I order several more [beer] throughout the night as each comedian makes their way to the stage, bringing fresh new and original "
        "Nah,  I'm fine":
            n "I decide never mind and sit in my chair in silence. And laughter. Okay, it's mostly laughter. Because comedy and stuff, you know"

    if drunkPoints == 0:
        n "Even though I didn't drink, I should make a note to tip my bartender, who is doing a fantastic job and deserves an applause."
    else:
        n "I make a mental note to tip my bartender generously tonight, who is doing a fantastic job and deserves an applause."

    n "(APPLAUD NOW)"

    n "I make that mental note as hard as I possibly can."

    n "Either way, I had an excellent time tonight!"

label cenaenters:
    
    n "Oh, hey, the show's about over now!"

    "Host" "Well, that about wraps out this show! Thank you all for coming out and--"

    "SMASH!"

    n "What was that?"

    play music "music/cenashort.mp3"

    n "Oh no. Not that."

    n "Anything but that."

    n "It's..."

    show cena stand

    n "WWE SUPERSTAR JOHN CENA?!"

    n "He wasn't on the list for QED! He wasn't on the list at all!"

    n "This seems like either ill timing or an incredibly bad plot reveal written at 2 AM, but here he is!"

    n "He saunters up to the stage like he owns the place, tossing a small washrag with his name on it into the crowd for some reason."

    n "Wait! He must be tired of grabbing all of the brass rings in WWE that he's here to take ours now too!"

    n "Well, I won't let him get away with this! I just started going to QEDs every Monday Night at 8 PM! He can't take that away from me!"

    n "It's time for a showdown!"

    show bg battle
    with pixellate

    play music "music/ff6.mp3"

label cenabattle:
    $ cenaAtk = renpy.random.randint(1,4) # (randomize between 1 and 2)
    if cenaHP <= 0:
        jump youwin

    if cenaAtk == 1:
        n "Cena hits you square on the face with some potato salad! Kinda gross, but kinda delicious!"
    elif cenaAtk == 2:
        n "Cena waves his hand in front of his face. You can't see him!"
        n "Oh, wait, there he is. Never mind."
    elif cenaAtk == 3:
        n "Cena offers you an Attitude Adjustment!"
        n "It's super effective!?!"
    elif cenaAtk == 4:
        n "Cena summons the CENATION!"
        n "You feel faintly guilty about what you're doing right now!"


    n "What should I do?"

    menu:

        "More Kazoo Solos!":
            stop music
            play music "music/kazoo3.mp3"

            n "PLEASE PAUSE FOR MORE KAZOOING"
            
            play music "music/ff6.mp3"

            n "Thank you."
            n "John Cena seems entirely confused, but it could also just be how he looks in general."
            n "(OHHHHH SICK BURN. He takes one damage!)"

            $ cenaHP -= 2
            jump cenabattle

        "Activate Smark Cannon!":
            n "I became to clap in a rhythmic pattern that is easily discenrable to the Superstar's ear."
            n "LET'S GO CENA CENA SUCKS! CLAP CLAP CLAPCLAPCLAP"
            n "He seems a little hurt by it."
            n "I sort of feel guilty now."
            n "Awww."
            $ cenaHP -= 1
            jump cenabattle

        "Summon Matt Chadourne":
            n "You summon the power of QED Mastermind Matt Chadourne!"
            n "He audibly sighes at John Cena to great effect!"
            n "John Cena staggers!"
            $ cenaHP -= 2
            jump cenabattle            

label youwin:

    hide cena
    with pixellate

    play music "music/victory.mp3"

    n "We did it! John Cena has been defeated by QED!"

    scene bg plinside
    with dissolve

    play music "music/whic.mp3"

    n "Well, I suppose it's the end for this week's show! What a great way to end it!"

    n "Except..."


    if (drunkPoints > artPoints):
        jump endingdrunk
    elif (artPoints > drunkPoints):
        jump endingart
    else:
        jump endingartgame


label endingart:
   
    scene white
    with dissolve

    n "This has totally turned into an art game ending."

    n "I suppose I could say that maybe it was a message about Commercialism, like Bubsy 3D and the James Turrell Experience..."

    n "Or maybe I could chalk it up as a zany way to keep the audience involved and compelled to come every week to QED."

    n "But in all honestly, I'm writing this like 15 minutes before I get off work and I needed an ending stat."

    n "But let's pass it off as genius and call it art! So, without further adieu..."

    n "Coming soon to Steam Early Access..."

    n "QED Comedy Laboratory! Every Monday Night at 8 PM at the Pilot Light! Be there or be... Uh, well not there!"

    n "We can't force you to come! It's a free country... FOR NOW."

    n "Next week is 'Dead Presidents' hosted by John Miller!"

    "---ART ENDING---"

    return 

label endingdrunk:
    scene black
    with dissolve

    n "It turns out I was just really drunk and fighting the air post-show."

    n "It's okay, though! They didn't mind it at all and even laughed about it!"

    n "In fact, it may even be a theme in the future: Drunk Miming!"

    n "So, until then..."

    n "QED Comedy Laboratory! Every Monday Night at 8 PM at the Pilot Light! Be there, laugh and drink [beer]!"
    n "Next week is 'Dead Presidents' hosted by John Miller!"

    "---DRUNK ENDING---"
    return

label endingartgame:
    scene red
    with dissolve

    n "You have now unlocked the best ending, by consuming the most beer and consuming the most art."

    n "You are the one. The one that will lead us to VICTORY"

    n "We have been waiting, from our far away star in the Xephon Galaxy for a video gamer exactly like you to take control..."

    n "And make the CHOICES we NEED."

    n "You have braved product branding, obtuse controls and even goats to make it to this point..."

    n "And now, we have a new mission for you."

    n "It's...."

    n "To come to QED Comedy Laboratory! Every Monday Night at 8 PM at the Pilot Light!"

    n "Next week is 'Dead Presidents' hosted by John Miller!"

    n "I wrote this ending in the 15 minutes before I got off work! Surprise!"

    "--- GO TO QED DAMMIT ENDING---"

    return